#!/bin/bash
echo "-------------------------"
echo "bangumi-danmu-fetcher"
echo "番剧弹幕获取助手"
echo "-------------------------"
echo "1--查看番剧清单"
echo "2--下载指定id的弹幕"
echo "-------------------------"
read -p "请选择:" choice
case ${choice} in
1)
    wget https://gitlab.com/bilibili-danmu/bilibili-bangumi-danmuku/raw/master/cid.list
    cat cid.list
    rm cid.list
    ;;
2)
    read -p "id:" id
    wget https://gitlab.com/bilibili-danmu/bilibili-bangumi-danmuku/raw/master/${id}.danmu
    ;;
esac