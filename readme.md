<h1 align="center"><a href="https://gitlab.com/bilibili-danmu/bangumi-danmu-fetcher/" target="_blank">Bilibili-danmu/bangumi-danmu-fetcher</a></h1>

> bangumi-danmu-fetcher是一款半自动弹幕获取软件。

![](https://github.com/luminocean/biliass/raw/master/demo.gif)

(这个gif是luminocean@github针对biliass制作的，在这里仅用于演示)

## 简介

2019年6月9日，哔哩哔哩下架数十部动漫，迫使观众在其他网站观看或下载番剧。bangumi-danmu-fetcher可以获取相关番剧的弹幕并引导使用者正确处理在视频中打开。

## 快速开始

请确保安装了wget。

1.下载`danmu.sh`

2.运行`danmu.sh`, 输入"1"查看番剧清单

3.以"欢迎来到实力至上主义的教室 03"为例，仔细寻找看到:

> #番剧sp号:6339
>
> #视频题目:"【7月】欢迎来到实力至上主义的教室 01【独家正版】"
>
> 93217234
>
> 93217677
>
> 93218372 《——这个就是第三集
>
> 93218829
>
> 93219205
>
> 93219698
>
> 93220353
>
> 93221073
>
> 23218293
>
> 93221804
>
> 93222382
>
> 93224375
>
> #番剧sp号:25739
>
> #视频题目:"【10月】关于我转生变成史莱姆这档事 01"

4.运行`danmu.sh`, 输入"2"下载制定id弹幕。在我们的例子中，id就是"93218372"。

5.打开"<http://tiansh.github.io/us-danmaku/bilibili/>"网页，上传弹幕文件(在本例子中是"93218372.danmu")

6.加载番剧视频，成功加载后选择"选择字幕"，并选中刚刚下载得到的.ass文件(在本例子中是"51640858.ass")

## 原理

bilibili-danmu项目在2019年6月2日保存了**所有番剧**的弹幕文件，并将其索引。本程序使用了该弹幕文件库(本来这个库就是我创建的！)作为弹幕的来源。

## FAQ

### 1.我发现了一个bug/可以改进的地方，怎么提交?

- ​	如果您知道该如何改正，请提交一个[merge request](https://gitlab.com/bilibili-danmu/core/merge_requests)。
- ​	如果您不知道该如何改正，请提交一个[issue](https://gitlab.com/bilibili-danmu/core/issues)。

> 请务必在issue里面提交:
>
> ​	1.您的发行版及版本信息;
>
> ​	2.程序的所有输出;
>
> ​	3.(可选)bug发生后的所有残留文件(请将整个程序文件夹用tar打包并上传).

## 许可证

> bangumi-danmu-fetcher使用GNU General Public License v3.0协议开源。
